import { Component } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  displayName;
fb_login_status = false;
userData: any;
    constructor(private afAuth: AngularFireAuth, public googlePlus: GooglePlus, public facebook:Facebook) {
      afAuth.authState.subscribe(user => {
        if (!user) {
          this.displayName = null;
          this.fb_login_status = false;
          return;
        }else{
          this.fb_login_status = true;
        }
        this.displayName = user.displayName;
              });
    }

    loginWithFB() {
        this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
          this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
            this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
          });
        });
      }

    signOut() {
      this.afAuth.auth.signOut();
    }

loginGoogle() {
  this.googlePlus.login({
    'webClientId':'150607608106-k6h71v6ockum587ns7a4m8ddd4foq969.apps.googleusercontent.com',
    'offline':true
  })
  .then(res =>{
    firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
    .then(suc=>{
      alert("LOGIN SUCESS")
    }).catch(ns=>{
      alert("NOT SUCESS")
    })
  })
}


}
