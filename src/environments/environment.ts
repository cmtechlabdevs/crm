// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBMtb--2s44msaryLC9PXTbDLu5Z_Q8YmA",
 authDomain: "fblogin-586fd.firebaseapp.com",
 databaseURL: "https://fblogin-586fd.firebaseio.com",
 projectId: "fblogin-586fd",
 storageBucket: "fblogin-586fd.appspot.com",
 messagingSenderId: "150607608106",
 appId: "1:150607608106:web:f136bc1bf9f19976"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
